terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.14"
    }
  }
}

provider "proxmox" {
  pm_api_url = "https://192.168.111.206:8006/api2/json"
  pm_debug   = true
}

variable "node" {
  type = string
}
variable "os" {
  type = string
}
variable "os_ossec" {
  type = string
}
variable "gateway" {
  type = string
}
variable "services" {
  type = list(string)
}
variable "users" {
  type = list(string)
}

variable "mac_addresses" {
  type = map(string)
}