#!/bin/sh

terraform init

echo "Start terraform plan"
terraform plan -out=plan -var-file="defaut_settings.tfvars"
echo "End of terraform plan"

echo "Start terraform apply"
terraform apply plan
echo "End of terraform apply"
