node     = "pve2"
os       = "local:vztmpl/ubuntu-22.04-standard_22.04-1_amd64.tar.zst"
os_ossec = "local:vztmpl/kali-linux.tar.xz"
gateway  = "192.168.1.1"
services = ["mariadb", "grafana", "loki"]
users    = ["guillaume", "ruddy", "amandine", "alexandre"]
mac_addresses = {
    "guillaume" = "B2:4A:9F:40:1A:93", "ruddy" = "F2:B2:86:60:D7:78", "amandine" = "CE:BC:71:EA:22:F8", "alexandre" = "0E:14:C1:BC:3D:49"
}