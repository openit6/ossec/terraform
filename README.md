# PROJECT OSSEC

## TERRAFORM

module to use: <https://registry.terraform.io/providers/Telmate/proxmox/latest/docs>

## Using this project

Before trying to use terraform, duplicate the file `source_env.template`, remove the extension and fill the missing values. Then use the command:
`source source_env` to set the credentials as environnement variables
Once you finished your terraform file: `terraform plan`
If everything is okay: `terraform apply`
