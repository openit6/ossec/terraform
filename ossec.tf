resource "proxmox_lxc" "ossec_server" {
  count        = 1
  target_node  = var.node
  hostname     = "ossec"
  vmid         = "250"
  ostemplate   = var.os_ossec
  password     = "supersecretpassword"
  unprivileged = true
  onboot       = true
  memory       = 1024
  start        = true

  ssh_public_keys = <<-EOT
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC3pC09mwskgKkidi+PU1PGqhNAg8FeMAiHIepSAeCjW5xwj9GV0TZSlm5vBZ+sukT/bMzCfCKZHuahmTS6gxPIUk1fZg1v6+jtyFA/MVZwbk2eitbtkTB34+sz6mqBgLJj7kqp010ZVZZA5FZ36qSotFjmYB9bZeW0Ff/l9nMOGGUZZTe3sK7LZAP5jdxyeR8q4I2TSJ8R0KD3ZqHL4Uwhj7141PwoJzWSsbNaXRoWfa9y+epv/HG5KFHRFo3+5mBkfOuYYf+ojlIpgyT1hU8XugD8BTa4JUA79TJT3QUGUXm12VTT9EB8bf+MJFOxa3J9KPdyNaTqj3QL5eVWYYbmcAz0ESybQR+t7nb3wW5P2tlZj+564cf98y55uug17BLQXbPWldf2D/zNB37n31c0fOB6EFzWtfR82xuicDWISfIZzD0OBBl0+Ib5z5lGcs6fSbExjz1g4KO1Czx5IRHfpPjTEpxYm5wCqNlMrZP1Lgxpy5r42fzU8l0RSrJwDZU= wslruddy@LAPTOP-H00TT9VL
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCgj1ADIy3WH5BwrUsF9zv6WFld2pYGeI931uMUEsUtB/61VEKUnerYvFaJMoVFn1LyL8czJW2gfp3z5uONdy5SdDTm/gUAdXyzW8XQzyE6JZwlwoyIXi7Go1noFa0Z3wE+tKhuXZmGxw/2R5PRDdxXUV8unfyge9sow42HR5YkR21A5VkqnuuXDCyiKO+/hmaQ/RZw6uZuyFl4IxMb9cBU3KJxrKNmlFkEv9YNTOfdUQc3R8DfdoEMPEr2ZjjP41hEhMjruh6VLYsVjCio2NZ8J3diTFUlnIKUrYm/1aVzxs3a9zqQ2XQtI+B6pn23Q/X+PErp9IjJ6c9vHcIx4w1BghX/zLo1nFbuXXe0XVK/mr13ktemkzIQzZOg3UcuAI6XCJcnIk4N5PiLHNJl+WGvafOCTGHoWhKsG2TKMEfgAVyM2AlL73VJFow6aRQeniwp7ii82Fk/9p//3vQ1OAl+wAkS76Il/CzCsVsTq4h0BLVedHZIc1/C+pj4f596iYE= root@DeployHostguillaume
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDTXAy/0hCGNFMF6zjkstKedSnkAU083roSPGsEdciAWlCuvZg+DkVHqwSOPbg+n/hU0WUbsUF10tu/J7GfVP/jgYoGfjdY7gXJzi69tmP/INf7UYi45gOF4jcaKE10f55CTtFIQewZ2U7jUZf1mJe1vDixkqwenVgVcnIEq/qA6M47I+dJ11yXNzB48CJeDKsl2+QAzEHfCbnNqWt4loLhEvJ+3e0FEPROEgM2NyeIKxYVmRRr8yn/jk/zaqndW30/DHUacqeyUnicdZHQ0FvlqzRZOIU7vocQRTkBAbe8X4wLWxkusosU22icsxHdcQBZyEl7kenPAI0R15RhkrkVAto4qEcJlp4EeAm4n3iuisCKWU8sZDLQAe36lu/2tJBPrEzgp8DWCwAiCf/zgbZze4t/Sddqfs32VlOsJvu8zZWmdtawAm+dvG8tvt0KqxNlI/nn1AFmdYzQkq5l6FR6XIrNR8IoSOe1sjznHksGeFkcW9iu7iJwplTBy0l2bq0= root@DeployHostruddy
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDsEQ2Fih9hfaSZyVLc8zLqUMsAaOmA2xoQNWdahBXd2BOmgFBLOnNbbKHKJI8g8eSzqN5l+4c6QD9vdx73nza3pHkzlnB3Ee2DfSAoDX+G4fJdnrodhovADI0rQo1I6zn1PZSXLVDrBsE92uzqHprKsM8kF0Rnw4D/JusJLpHI4onzAXEoJExdMbDslYBNLuIXBI4x1MMB/BOzXYjvKiqlvTPnUBN7c5KCp+d3uw1+pEDtwCcOMbgY7pesK95Y3PmKdhqAUJJ9cpqG/pppGEcC1/bCzES8obKoWV6WZkBl3SEEmAOHMjClCYYG281o1qn1DWnzaTAiL2ssBW7X1+wjqHlrPeO+RVClpRjL9N126moIf62iVLGHuRNiXRMIosrdcNq/+Tc/Zxzgm/IIH1JlFxOChQBgcTqnGdVQMCfZj33AD181D9zrqe2dI8X9XF5+XX8lY21kz1PH697tcSdWbiBbA+9C+/3D0nsPiq9wG/tu2nSpLDg5qhbtUXrF8zk= kyabe@PCKyabe
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDo3Zs+uwxOQCOpVK5EvDYJjZhGrTZMmxJQ6cwaplgeJ1zicpo5OQoOU95mRuM9lPcs75vQH0W5JpUgOT9/DaN3ESfj4hFx9ECUeT27YE7qA58dCJjCvQhBDZJ+gXeo1uvN04pX6S/4+PkuZdeSNfwSHBl3uJSNXOCzDtDvFWkPzWqrVJTF+IaZMcu3BW7mwsRbDO0X/vyOf38x9O4XrgkbFhwv0OaMB8SreMiq+DkNmv+km9U9If1ZRcC4lakyUvK9pAvHSPJitV0eMF0MmVyjPsQqLS6YoX/N1ZDdxuEhPyfi05ML2njJ58lx34eiZUYpFXKQ2JwB0codiqTCeKSfjDspkStQSYvWMbGDefuDTv+RXHR9LRD51u4fzrYoD58Lxuo+OH4QtFp8dCNE4gHoxHQa3NUfhIfUWeFlFH2phc/56+a+fhapjEL4V8Ch7xY4eg5ICtsdTQdlJuzbZMEvXj9R+R6mQMwAhrmNUxUOPH/nmNKrKc6OS6y9C5DmntE= alex3@DESKTOP-HBM46R5

EOT


  // Terraform will crash without rootfs defined
  rootfs {
    storage = "local-zfs"
    size    = "8G"
  }

  network {
    name   = "eth0"
    bridge = "vmbr2"
    ip     = "192.168.1.250/24"
    gw     = var.gateway
  }
}